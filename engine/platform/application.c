#include "application.h"
#include <stdlib.h>

float Random()
{
    return (float)rand() / RAND_MAX;
}

float RandomFloat(float min, float max)
{
    return min + (float)rand() / RAND_MAX * (max - min);
}

int RandomInt(int min, int max)
{
    return min + rand() % (max - min);
}

