#include "asset.h"

#ifdef _WIN32
#include <windows.h>
#elif linux
#include <libgen.h>
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "print.h"

// TODO: support for "resource packs"

static FILE **_asset_files = NULL;
static int _asset_count = 0;

typedef struct assetPath_t
{
    const char *path;
    struct assetPath_t *next;
} AssetPath;
AssetPath *_asset_paths = NULL;

const char pathSeparator =
#ifdef _WIN32
    '\\';
#else
    '/';
#endif

#define MIN(a,b) ((a) < (b) ? (a) : (b))

size_t _Path_Join(const char *a, const char *b, char *buf, size_t buflen)
{
    size_t alen = strlen(a);
    size_t blen = strlen(b);
    size_t len = alen + blen + 2;
    if (len > buflen) return 0;
    memcpy(buf, a, alen);
    buf[alen] = pathSeparator;
    memcpy(buf+alen+1, b, blen);
    buf[len-1] = '\0';
    return len;
}

int Asset_Open(const char *_name)
{
    if (!_name) return -1;
    DebugFormat("Finding asset %s", _name);

    char *name = malloc(strlen(_name)+1);
    AssetPath *p = _asset_paths;
    while (p) {
        size_t pathlen = strlen(p->path);
        size_t namelen = strlen(_name);
        size_t fullen = pathlen + 1 + namelen + 1;
        name = realloc(name, fullen);
        fullen = _Path_Join(p->path, _name, name, fullen);
#ifdef _WIN32
        if (GetFileAttributes(name) == 0xFFFFFFFF)
            break;
#else
        if (access(name, F_OK) != -1) {
            break;
        }
#endif

        p = p->next;
    }
    // Was not found in asset paths
    if (!p) {
        DebugFormat("Not found in any asset path %s", _name);
        strcpy(name, _name);
    }

    DebugFormat("Opening asset from %s", name);

    // TODO: find file
    FILE *f = fopen(name, "rb");
    if (!f) return -1;
    _asset_count++;
    if (!_asset_files)
        _asset_files = malloc(sizeof(FILE*));
    else
        _asset_files = realloc(_asset_files, sizeof(FILE*)*_asset_count);

    _asset_files[_asset_count-1] = f;

    return _asset_count-1;
}

int Asset_Valid(int asset)
{
    return (asset >= 0 && asset < _asset_count);
}

size_t Asset_Size(int asset)
{
    if (!Asset_Valid(asset)) return 0;
    FILE *f = _asset_files[asset];
    long pos = ftell(f);
    fseek(f, 0L, SEEK_END);
    long ret = ftell(f);
    fseek(f, pos, SEEK_SET);
    return ret;
}

size_t Asset_Read(int asset, void *ptr, size_t size, size_t nmemb)
{
    if (!Asset_Valid(asset)) return 0;
    FILE *f = _asset_files[asset];
    if (!f) return 0;
    return fread(ptr, size, nmemb, f);
}

size_t Asset_ReadString(int asset, char* buffer)
{
    if (!Asset_Valid(asset)) return 0;
    FILE *file = _asset_files[asset];
    //SkipWhiteSpace(file);

    int c = fgetc(file);
    int pos = 0;

    while (c != EOF && (char)c != ' ')
    {
        buffer[pos++] = (char)c;
        c = fgetc(file);
    }
    buffer[pos] = 0;

    printf("ReadString %s\n", buffer);

    return pos;
}

void Asset_Close(int asset)
{
    if (!Asset_Valid(asset)) return;
    FILE *f = _asset_files[asset];
    fclose(f);
    _asset_files[asset] = NULL;
}

void Asset_Register_Path(const char *path)
{
    if (!path) return;
    DebugFormat("Registering asset path %s", path);
    AssetPath *next = _asset_paths;
    _asset_paths = malloc(sizeof(AssetPath));
    char *buf = malloc(strlen(path)+1);
    strcpy(buf, path);
    _asset_paths->path = buf;
    _asset_paths->next = next;
}

int _getexedir(char *buf, size_t buflen)
{
#ifdef _WIN32
    int bytes = GetModuleFileName(NULL, buf, buflen);
    if (bytes == 0)
        return -1;
    // TODO: split path to get dirname
#define DRIVELEN 8
    char drive[DRIVELEN];
#define DIRLEN 256
    char dir[DIRLEN];
    int r = _splitpath_s(buf, drive, DRIVELEN, dir, DIRLEN, NULL, 0, NULL, 0);
    if (r != 0) return -1;
    int drivelen = strlen(drive);
    int dirlen = strlen(dir);
    strncpy(buf, drive, drivelen);
    strncpy(buf + drivelen, dir, dirlen);
    buf[drivelen + dirlen - 1] = '\0'; // Ignore trailing backslash
    return drivelen + dirlen;
#elif linux
    char szTmp[32];
    sprintf(szTmp, "/proc/%d/exe", getpid());
    int bytes = MIN(readlink(szTmp, buf, buflen), buflen - 1);
    if (bytes >= 0)
        buf[bytes] = '\0';
    buf = dirname(buf);
    return strlen(buf);
#else
    return -1; // Platform not supported
#endif
}

void Asset_Register_Default_Path()
{
#define BUFLEN 256
    char exedir[BUFLEN];
    int len = _getexedir(exedir, BUFLEN);
    if (len > 0) {
        len = _Path_Join(exedir, "assets", exedir, BUFLEN);
        if (len > 0)
            Asset_Register_Path(exedir);
    }
}
