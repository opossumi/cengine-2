#include "mesh.h"
#include "asset.h"
#include "application.h"
#include "renderer.h"

#include "print.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

const int MAX_MESH_COUNT = 100;
Mesh meshes[100];
char meshExists[100] = { 0 };

int Mesh_Create()
{
    for (int i = 0; i < MAX_MESH_COUNT; ++i)
    {
        if (meshExists[i] == 0)
        {
            meshExists[i] = 1;
            meshes[i] = (Mesh) { 0 };
            return i;
        }
    }

    return 0;
}

Mesh Mesh_Get(int id)
{
    return meshes[id];
}

void Mesh_Destroy(int id)
{
    Mesh_DeleteFromGPU(id);
    
    meshExists[id] = 0;
    if (meshes[id].vertexArray != 0)
        free(meshes[id].vertexArray);
    if (meshes[id].uvArray != 0)
        free(meshes[id].uvArray);
    if (meshes[id].indexArray != 0)
        free(meshes[id].indexArray);
}

void Mesh_SetVertexCount(int id, VertexPtrType vertexCount)
{
    DebugFormat("Mesh_SetVertexCount %u", (unsigned int)vertexCount);
    meshes[id].vertexArray = (Vertex*)malloc(sizeof(Vertex) * vertexCount);
    meshes[id].vertexCount = vertexCount;
}

void Mesh_EnableUVs(int id)
{
    DebugFormat("Mesh_EnableUVs %u", (unsigned int)meshes[id].vertexCount);
    meshes[id].uvArray = (UV*)malloc(sizeof(UV) * meshes[id].vertexCount);
}

void Mesh_SetIndexCount(int id, IndexCountType indexCount)
{
    DebugFormat("Mesh_SetIndexCount %u", (unsigned int)indexCount);
    meshes[id].indexArray = (VertexPtrType*)malloc(sizeof(VertexPtrType) * indexCount);
    meshes[id].indexCount = indexCount;
}

void Mesh_SetVertex(int id, int i, Vertex vertex)
{
    meshes[id].vertexArray[i] = vertex;
}

void Mesh_SetUV(int id, int i, UV uv)
{
    meshes[id].uvArray[i] = uv;
}

void Mesh_SetIndex(int id, int i, VertexPtrType index)
{
    meshes[id].indexArray[i] = index;
}

void Mesh_SetGPUBuffers(int id, unsigned int gpuVertexBuffer, unsigned int gpuIndexBuffer)
{
    meshes[id].gpuVertexBuffer = gpuVertexBuffer;
    meshes[id].gpuIndexBuffer = gpuIndexBuffer;
}

//void SkipWhiteSpace(FILE* file)
//{
//    int c = fgetc(file);
//
//    while (c != EOF && isspace(c))
//    {
//        c = fgetc(file);
//    }
//    ungetc(c, file);
//}

int Mesh_Load(const char* filepath, const char* filter)
{
    int asset = Asset_Open(filepath);

    if (asset == -1)
    {
        return -1;
    }

    char buffer[256];
    //fread_s(buffer, 256, 1, 256, file);
    
    int meshId = -1;

    int returnMesh = -1;

    while (1)
    {
        Asset_ReadString(asset, buffer);
        if (strcmp(buffer, "Mesh") == 0)
        {
            if (returnMesh != -1)
                break;

            meshId = Mesh_Create();

            // Mesh name
            Asset_ReadString(asset, buffer);

            if (filter == 0 || strcmp(buffer, filter) == 0)
                returnMesh = meshId;
        }
        else if (strcmp(buffer, "Vertices") == 0)
        {
            // Vertex count
            Asset_ReadString(asset, buffer);

            int vertexCount = atoi(buffer);
            Mesh_SetVertexCount(meshId, (VertexPtrType)vertexCount);
                
            // Read vertex data
            Asset_Read(asset, meshes[meshId].vertexArray, sizeof(Vertex), vertexCount);
        }
        else if (strcmp(buffer, "UVs") == 0)
        {
            //SkipWhiteSpace(file);
            Mesh_EnableUVs(meshId);

            // Read UV data
            Asset_Read(asset, meshes[meshId].uvArray, sizeof(UV), meshes[meshId].vertexCount);

            for (int i = 0; i < meshes[meshId].vertexCount; ++i)
            {
                meshes[meshId].uvArray[i].y = 1.0f - meshes[meshId].uvArray[i].y;
            }

            for (int i = 0; i < 10; ++i)
                printf("%f %f\n", meshes[meshId].uvArray[i].x, meshes[meshId].uvArray[i].y);

        }
        else if (strcmp(buffer, "Indices") == 0)
        {
            // Index count
            Asset_ReadString(asset, buffer);
            
            int indexCount = atoi(buffer);
            Mesh_SetIndexCount(meshId, (IndexCountType)indexCount);
               
            //SkipWhiteSpace(file);

            // Read index data
            Asset_Read(asset, meshes[meshId].indexArray, sizeof(VertexPtrType), indexCount);
        }
        else
            break;


            //printf("%u %u %u", meshes[meshId].indexArray[0], meshes[meshId].indexArray[1], meshes[meshId].indexArray[2]);
            //printf("%f %f %f", meshes[meshId].vertexArray[0].x, meshes[meshId].vertexArray[0].y, meshes[meshId].vertexArray[0].z);
    }

    Asset_Close(asset);

    return meshId;
}
