#ifdef _WIN32

#ifndef UNICODE
#define UNICODE
#endif 

#include <windows.h>
#include "application.h"
#include "asset.h"
#include "renderer.h"
#include "renderer_windows.h"
#include "input.h"
#include <stdio.h>


// ------------------ input.h -----------------------

HWND __hwnd;
void Input_GetMousePosition(float* x, float* y)
{
    POINT cursorPos;
    GetCursorPos(&cursorPos);
    ScreenToClient(__hwnd, &cursorPos);
    *x = (float)cursorPos.x;
    *y = (float)cursorPos.y;
}

int Input_GetButtonState(int button)
{
    return ((GetKeyState(button) & 0x8000) != 0) ? 1 : 0;
}

// --------------------------------------------------

void GetScreenSize(int *width, int* height)
{
    RECT windowRect;

    GetClientRect(__hwnd, &windowRect);

    *width = windowRect.right - windowRect.left;
    *height = windowRect.bottom - windowRect.top;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    case WM_SIZE:
    {
        ResizeViewport(LOWORD(lParam), HIWORD(lParam));
        return 0;
    }

    //case WM_PAINT:
    //{
    //    PAINTSTRUCT ps;
    //    HDC hdc = BeginPaint(hwnd, &ps);
    //    HBRUSH brush = CreateSolidBrush(RGB(150, 1, 151));
    //
    //    FillRect(hdc, &ps.rcPaint, brush);//(HBRUSH)(COLOR_WINDOW + 1));
    //
    //    DeleteObject(brush);
    //
    //    EndPaint(hwnd, &ps);
    //}

    default:
        break;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE h, PWSTR pCmdLine, int nCmdShow)
{
    // Register the window class.

    WNDCLASS wc = { 0 };

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = L"C Engine";

    RegisterClass(&wc);

    // Create the window.
    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles.
        wc.lpszClassName,                     // Window class
        L"C Engine",    // Window text
        WS_OVERLAPPEDWINDOW,            // Window style

                                        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
        );

    if (hwnd == NULL)
    {
        return 1;
    }

    __hwnd = hwnd;

    AllocConsole();
    freopen_s((FILE**)stdout, "CONOUT$", "w", stdout);

    ShowWindow(hwnd, nCmdShow);

    HGLRC hglrc;
    InitRenderer(hwnd, &hglrc);

    Asset_Register_Default_Path();
    Asset_Register_Path("C:\\Users\\CFE\\Desktop\\dev\\cengine2\\assets");
    Init();

    HDC hdc = GetDC(hwnd);

    // Run the message loop.

    LARGE_INTEGER tickRate;
    QueryPerformanceFrequency(&tickRate);
    LARGE_INTEGER lastTick;
    QueryPerformanceCounter(&lastTick);

    MSG msg = { 0 };
    int run = 1;
    float deltaTime = 0;
    int targetFPS = 60;
    while (run == 1)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                run = 0;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            ClearScreen();
            Update(deltaTime);
            Draw();
            SwapBuffers(hdc);

            LARGE_INTEGER tick;
            QueryPerformanceCounter(&tick);

            while ((tick.QuadPart - lastTick.QuadPart) * targetFPS < tickRate.QuadPart - tickRate.QuadPart / 10)
            {
                Sleep(1);
                QueryPerformanceCounter(&tick);
            }

            deltaTime = (float)((double)(tick.QuadPart - lastTick.QuadPart) / tickRate.QuadPart);

            lastTick = tick;
            //printf("%f\n", 1.0f / deltaTime);
        }
    }

    DeinitRenderer(hwnd, hglrc);

    FreeConsole();

    return 0;
}


#endif
