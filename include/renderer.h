#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "math2.h"

const char* GetGPUVendorName();
const char* GetGPUName();
void PrintErrors();

void SetPerspectiveMatrix(float fovY, float aspect, float zNear, float zFar);
void ResizeViewport(int width, int height);

void ClearScreen();

void SetCameraTransformMatrix(Matrix matrix);
void SetCameraTransform(Transform transform);

void Mesh_UploadToGPU(int meshID);
void Mesh_DeleteFromGPU(int meshID);

void Draw_Mesh(int meshID, Transform transform);
void Draw_Quad(float x, float y, float width, float height);
void Draw_Text(float x, float y, char* text);

#endif