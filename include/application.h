// global
float Random();
float RandomFloat(float min, float max);
int RandomInt(int min, int max);
void GetScreenSize(int* width, int* height);

// user
void Init();
void Update(float deltaTime);
void Draw();
