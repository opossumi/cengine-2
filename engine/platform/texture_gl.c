#include "texture.h"
#ifdef _WIN32
#include <windows.h>
#endif
#include <stdlib.h>
#include "asset.h"
#include "gl.h"
#include "print.h"
#include "renderer.h"

#include <stdio.h>
#include "../engine/lodepng.h"

int GetTextureFilter(TextureFilterMode filterMode, int mipmap)
{
    int filter;
    if (mipmap)
    {
        switch (filterMode)
        {
        case TextureFilter_Point:
            filter = GL_NEAREST_MIPMAP_NEAREST;
            break;
        case TextureFilter_Bilinear:
            filter = GL_LINEAR_MIPMAP_NEAREST;
            break;
        case TextureFilter_Trilinear:
            filter = GL_LINEAR_MIPMAP_LINEAR;
            break;
        }
    }
    else
    {
        switch (filterMode)
        {
        case TextureFilter_Point:
            filter = GL_NEAREST;
            break;
        case TextureFilter_Bilinear:
        case TextureFilter_Trilinear:
            filter = GL_LINEAR;
            break;
        }
    }
    return filter;
}

int Texture_Load(const char* fname)
{
    unsigned char* img;
    unsigned int width, height;

    int asset = Asset_Open(fname);
    if (asset == -1)
        return 0;
    size_t size = Asset_Size(asset);
    unsigned char *data = malloc(size);
    Asset_Read(asset, data, 1, size);
    unsigned int error = lodepng_decode_memory(&img, &width, &height,
            data, size, LCT_RGB, 8);

    int textureId = -1;

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    int mipmap = 1;
    TextureFilterMode filterMode = TextureFilter_Trilinear;
    int glFilter = GetTextureFilter(filterMode, mipmap);

    if (mipmap)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
        glTexEnvf(GL_TEXTURE_FILTER_CONTROL, GL_TEXTURE_LOD_BIAS, -1);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMode == TextureFilter_Point ? GL_NEAREST : GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
    
    PrintErrors();

    printf("Created texture %i, width: %i, height: %u\n", textureId, width, height);

    return textureId;
}

void Texture_Bind(int textureId)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);
}

void Texture_Unbind()
{
    glDisable(GL_TEXTURE_2D);
}
