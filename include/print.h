#ifndef _PRINT_H_
#define _PRINT_H_

void DebugLine(const char* str);
void DebugFormat(const char* string, ...);

#endif
