#include "print.h"

#include <stdio.h>
#include <stdarg.h>

void DebugLine(const char* str)
{
    printf(str);
    printf("\n");
}

void DebugFormat(const char* format, ...)
{
    va_list arglist;
    va_start(arglist, format);
    vprintf(format, arglist);
    va_end(arglist);
    printf("\n");
}
