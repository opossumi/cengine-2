#ifndef _INPUT_H_
#define _INPUT_H_

void Input_GetMousePosition(float* x, float* y);
int Input_GetButtonState(int button);

#endif