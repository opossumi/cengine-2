#ifndef _MESH_H_
#define _MESH_H_

typedef struct
{
    float x, y, z;
} Vertex;

typedef struct
{
    float x, y;
} UV;

typedef unsigned short VertexPtrType;
typedef unsigned int IndexCountType;

typedef struct
{
    VertexPtrType vertexCount;
    Vertex* vertexArray;
    UV* uvArray;
    IndexCountType indexCount;
    VertexPtrType* indexArray;

    unsigned int gpuVertexBuffer;
    unsigned int gpuIndexBuffer;
} Mesh;

int Mesh_Create();
void Mesh_SetVertexCount(int id, VertexPtrType vertexCount);
void Mesh_EnableUVs(int id);
void Mesh_SetIndexCount(int id, IndexCountType indexCount);
//void Mesh_SetDataSize(int id, VertexPtrType vertexCount, IndexCountType indexCount);
//void Mesh_SetData(int id, Vertex* vertexArray, int* indexArray);
void Mesh_Destroy(int id);
Mesh Mesh_Get(int id);
void Mesh_SetVertex(int id, int i, Vertex vertex);
void Mesh_SetUV(int id, int i, UV uv);
void Mesh_SetIndex(int id, int i, VertexPtrType index);
void Mesh_SetGPUBuffers(int id, unsigned int gpuVertexBuffer, unsigned int gpuIndexBuffer);
int Mesh_Load(const char* filepath, const char* filter);

#endif
