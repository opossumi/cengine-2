#include "renderer.h"
#include "application.h"
#include "mesh.h"
#include "texture.h"
#include "print.h"
#include "input.h"
#include "math2.h"

#include <stdio.h>

float x = -1;
float z = 0;

int car1mesh;
int car2mesh;
int characterMesh;

int car1tex;
int car2tex;
int grassTex;
int characterTex;

int groundMesh;

float fpsTime = 0;
int frames = 0;
float fps = 0;

float mouse_x = 0;
float mouse_y = 0;

Transform cameraTransform;
Transform carTransform;
Transform groundTransform;
Transform characterTransform;

void Init()
{
    car1mesh = Mesh_Load("car-1.mesh", 0);
    car2mesh = Mesh_Load("car-2.mesh", 0);
    characterMesh = Mesh_Load("bomber.mesh", "bomber_LOD0");

    car1tex = Texture_Load("car-1.png");
    car2tex = Texture_Load("car-2.png");
    grassTex = Texture_Load("grass-1.png");
    characterTex = Texture_Load("bomber.png");

    Mesh_UploadToGPU(car1mesh);
    Mesh_UploadToGPU(car2mesh);
    Mesh_UploadToGPU(characterMesh);

    groundMesh = Mesh_Create();
    
    Mesh_SetVertexCount(groundMesh, 4);

    Mesh_SetVertex(groundMesh, 0, (Vertex) {-1, 0, 1 });
    Mesh_SetVertex(groundMesh, 1, (Vertex) { 1, 0, 1 });
    Mesh_SetVertex(groundMesh, 2, (Vertex) { 1, 0,-1 });
    Mesh_SetVertex(groundMesh, 3, (Vertex) {-1, 0,-1 });

    Mesh_EnableUVs(groundMesh);
    Mesh_SetUV(groundMesh, 0, (UV) { 0, 0 });
    Mesh_SetUV(groundMesh, 1, (UV) {10, 0 });
    Mesh_SetUV(groundMesh, 2, (UV) {10, 10});
    Mesh_SetUV(groundMesh, 3, (UV) { 0, 10});

    Mesh_SetIndexCount(groundMesh, 6);
    Mesh_SetIndex(groundMesh, 0, 0);
    Mesh_SetIndex(groundMesh, 1, 1);
    Mesh_SetIndex(groundMesh, 2, 2);
    Mesh_SetIndex(groundMesh, 3, 2);
    Mesh_SetIndex(groundMesh, 4, 3);
    Mesh_SetIndex(groundMesh, 5, 0);

    Mesh_UploadToGPU(groundMesh);

    cameraTransform.rotation = QuaternionFromEuler(45, 0, 0);
    cameraTransform.scale = (Vector3) { 1, 1, 1 };
    cameraTransform.position = (Vector3) { 0, 150, 180 };

    SetCameraTransform(cameraTransform);

    groundTransform.position = (Vector3) { 0, -10, 0 };
    groundTransform.scale = (Vector3) { 200, 200, 200 };

    characterTransform.position = (Vector3) { 0, 0, 0 };
    characterTransform.scale = (Vector3) { 10, 10, 10 };

    Input_GetMousePosition(&mouse_x, &mouse_y);
}

void Update(float deltaTime)
{
    frames++;

    fpsTime += deltaTime;
    if (fpsTime >= 1.0f)
    {
        fps = (float)frames / fpsTime;
        DebugFormat("fps: %f\n", fps);
        fpsTime -= 1.0f;
        frames = 0;
    }

    //Mesh car = Mesh_Get(car1mesh);
    //for (int i = 0; i < car.vertexCount; ++i)
    //{
    //    car.vertexArray[i].x += RandomFloat(-.2f, .2f);
    //    car.vertexArray[i].y += RandomFloat(-.2f, .2f);
    //    car.vertexArray[i].z += RandomFloat(-.2f, .2f);
    //}

    x += deltaTime * 90;
    z += 0.1f;

    float prev_x = mouse_x;
    float prev_y = mouse_y;
    Input_GetMousePosition(&mouse_x, &mouse_y);
    //cam_rot_x -= .2f * (mouse_y - prev_y);
    //cam_rot_y -= .2f * (mouse_x - prev_x);
    
    Vector3 move = { 0 };

    if(Input_GetButtonState((int)'W'))
        move.z -= 1;
    if (Input_GetButtonState((int)'S'))
        move.z += 1;
    if (Input_GetButtonState((int)'A'))
        move.x -= 1;
    if (Input_GetButtonState((int)'D'))
        move.x += 1;

    move.x *= 100 * deltaTime;
    move.z *= 100 * deltaTime;

    if (Input_GetButtonState(0x01))
    {
        Vector3 rotate = { 0 };
        rotate.x = .2f * (mouse_y - prev_y);
        rotate.y = .2f * (mouse_x - prev_x);

        cameraTransform.rotation = QuaternionMultiply(
            cameraTransform.rotation,
            QuaternionFromEuler(0, rotate.y, 0));
        cameraTransform.rotation = QuaternionMultiply(
            QuaternionFromEuler(rotate.x, 0, 0),
            cameraTransform.rotation);
    }

    Quaternion rot = cameraTransform.rotation;
    QuaternionInvert(&rot); // Why does it need invert?
    move = Vector3RotateByQuaternion(move, rot);

    cameraTransform.position = Vector3Add(
        cameraTransform.position, 
        move);

    SetCameraTransform(cameraTransform);
}

void Draw()
{
    //Draw_Quad(x, 0, .1f, .1f);
    //Texture_Unbind();
    //Draw_Mesh(car1mesh, -20, -10, 0, -90, 0, x, .05f, .05f, .05f);
    //Texture_Bind(car1tex);
    //Draw_Mesh(car1mesh, 20, -10, 0, -90, 0, x, .05f, .05f, .05f);

    carTransform.scale = (Vector3){ .05f, .05f, .05f };
    carTransform.position.y = -10;

    for (int i = 0; i < 100; ++i)
    {
        carTransform.position.x = -100 + 200 * (float)(i % 10) / 10;
        carTransform.position.z = -100 + 200 * (float)(i / 10) / 10;
        carTransform.rotation = QuaternionFromEuler(-90, 0, x / (1 + i));
        if (i % 2 == 0)
        {
            Texture_Bind(car1tex);
            Draw_Mesh(car1mesh, carTransform);
        }
        else
        {
            Texture_Bind(car2tex);
            Draw_Mesh(car2mesh, carTransform);
        }
    }

    //Texture_Bind(car2tex);
    //Draw_Mesh(car2mesh, 20, -10, -100, -90, 0, x*.5f, .1f, .1f, .1f);
    //
    Texture_Bind(grassTex);
    Draw_Mesh(groundMesh, groundTransform);

    Texture_Bind(characterTex);
    Draw_Mesh(characterMesh, characterTransform);

    Texture_Unbind();

    char fpsText[32];
#ifdef _WIN32
    sprintf_s(fpsText, 32, "FPS: %i", (int)(fps + 0.5f));
#else
    snprintf(fpsText, 32, "FPS: %i", (int)(fps + 0.5f));
#endif

    Draw_Text(10, 10, fpsText);
}
