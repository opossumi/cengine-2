#include <stdio.h>
#include "fbx\ofbx.h"

#include "math2.h"

float* buffer;
unsigned short* indexbuffer;

Matrix GetMatrix(ofbx::Matrix mat)
{
    Matrix m;
    m.m0  = (float)mat.m[0];
    m.m1  = (float)mat.m[1];
    m.m2  = (float)mat.m[2];
    m.m3  = (float)mat.m[3];
    m.m4  = (float)mat.m[4];
    m.m5  = (float)mat.m[5];
    m.m6  = (float)mat.m[6];
    m.m7  = (float)mat.m[7];
    m.m8  = (float)mat.m[8];
    m.m9  = (float)mat.m[9];
    m.m10 = (float)mat.m[10];
    m.m11 = (float)mat.m[11];
    m.m12 = (float)mat.m[12];
    m.m13 = (float)mat.m[13];
    m.m14 = (float)mat.m[14];
    m.m15 = (float)mat.m[15];
    return m;
}

int main()
{
    FILE* fp;
    fopen_s(&fp, "C:\\Users\\CFE\\Desktop\\dev\\cengine2\\assets\\bomber_mesh.FBX", "rb");
    if (!fp)
        return 0;

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    auto* content = new ofbx::u8[file_size];
    fread(content, 1, file_size, fp);

    ofbx::IScene* scene = ofbx::load((ofbx::u8*)content, file_size);

    int mesh_count = scene->getMeshCount();

    FILE* outfile;
    fopen_s(&outfile, "C:\\Users\\CFE\\Desktop\\dev\\cengine2\\assets\\bomber.mesh", "wb");

    buffer = new float[100000];
    indexbuffer = new unsigned short[100000];

    for (int i = 0; i < mesh_count; ++i)
    {
        const ofbx::Mesh* mesh = scene->getMesh(i);
        fprintf(outfile, "Mesh %s ", mesh->name);

        const ofbx::Geometry* geom = mesh->getGeometry();
        ofbx::Vec3 scale = mesh->getLocalScaling();
        printf("mesh scale %f %f %f\n", scale.x, scale.y, scale.z);
        scale = geom->getLocalScaling();
        printf("geom scale %f %f %f\n", scale.x, scale.y, scale.z);

        printf("name %s\n", mesh->name);

        Matrix matrix = GetMatrix(mesh->getGeometricMatrix());

        double* mat = mesh->getGeometricMatrix().m;
        printf("transform\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n",
            mat[0], mat[1], mat[2], mat[3],
            mat[4], mat[5], mat[6], mat[7],
            mat[8], mat[9], mat[10], mat[11],
            mat[12], mat[13], mat[14], mat[15]);

        int vertexCount = geom->getVertexCount();
        const ofbx::Vec3* vertices = geom->getVertices();
        const ofbx::Vec2* uvs = geom->getUVs(0);

        int indexCount = vertexCount;//geom->GetOriginalIndexCount();
        //const int* indices = //geom->GetOriginalIndices();

        //printf("%i %i %i\n", vertexCount, indexCount);

        // Vertex data
        fprintf(outfile, "Vertices %i ", vertexCount);
        for (int j = 0; j < vertexCount; ++j)
        {
            Vector3 pos = { (float)vertices[j].x, (float)vertices[j].y, (float)vertices[j].z };
            Vector3Transform(&pos, matrix);
            buffer[j * 3 + 0] = pos.x;
            buffer[j * 3 + 1] = pos.y;
            buffer[j * 3 + 2] = pos.z;
        }

        printf("%f %f %f\n", (float)vertices[0].x, (float)vertices[0].y, (float)vertices[0].z);
        fwrite(buffer, sizeof(float) * 3, vertexCount, outfile);

        // UV data
        fprintf(outfile, "UVs ");
        for (int j = 0; j < vertexCount; ++j)
        {
            buffer[j * 2 + 0] = (float)uvs[j].x;
            buffer[j * 2 + 1] = (float)uvs[j].y;
        }

        for (int j = 0; j < 10; ++j)
            printf("uv %f %f\n", buffer[j * 2 + 0], buffer[j * 2 + 1]);

        fwrite(buffer, sizeof(float) * 2, vertexCount, outfile);

        // Index data
        // TODO: Remove duplicate vertices
        fprintf(outfile, "Indices %i ", indexCount);
        //for (int j = 0; j < indexCount; ++j)
        //{
        //    int idx = indices[j];
        //    idx = idx < 0 ? -idx - 1 : idx;
        //    indexbuffer[j] = (unsigned short)idx;
        //}
        
        for (int j = 0; j < indexCount; ++j)
        {
            indexbuffer[j] = (unsigned short)j;
        }

        //printf("%u %u %u\n", indexbuffer[0], indexbuffer[1], indexbuffer[2]);
        //
        //
        fwrite(indexbuffer, sizeof(unsigned short), indexCount, outfile);
    }

    fclose(outfile);

    delete[] content;
    fclose(fp);

    getchar();
    return 0;
}