#ifndef _ASSET_H_
#define _ASSET_H_

#include <stddef.h>

int Asset_Open(const char *name);
int Asset_Valid(int asset);
size_t Asset_Size(int asset);
size_t Asset_Read(int asset, void *ptr, size_t size, size_t nmemb);
size_t Asset_ReadString(int asset, char* buf);
void Asset_Close(int asset);
void Asset_Register_Path(const char *path);
void Asset_Register_Default_Path();

#endif // _ASSET_H_
