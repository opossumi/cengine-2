#ifndef _TEXTURE_H_
#define _TEXTURE_H_

typedef enum
{
    TextureFilter_Point,
    TextureFilter_Bilinear,
    TextureFilter_Trilinear,
} TextureFilterMode;

int Texture_Load(const char* fname);
void Texture_Bind(int textureId);
void Texture_Unbind();

#endif