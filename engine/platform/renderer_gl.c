#include "renderer.h"
#include "mesh.h"
#include "print.h"
#include "math2.h"
#include "application.h"

// http://galogen.gpfault.net/galogen-web.html
#include "gl.h"
#include <math.h>

#include <stdio.h>

#include "../stb_easy_font.h"

const char* GetErrorString(GLenum const err)
{
    switch (err)
    {
        // opengl 2 errors (8)
    case GL_NO_ERROR:
        return "GL_NO_ERROR";

    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";

    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";

    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";

    case GL_STACK_OVERFLOW:
        return "GL_STACK_OVERFLOW";

    case GL_STACK_UNDERFLOW:
        return "GL_STACK_UNDERFLOW";

    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";

    //case GL_TABLE_TOO_LARGE:
    //    return "GL_TABLE_TOO_LARGE";
    //
    //    // opengl 3 errors (1)
    //case GL_INVALID_FRAMEBUFFER_OPERATION:
    //    return "GL_INVALID_FRAMEBUFFER_OPERATION";
    //
    //    // gles 2, 3 and gl 4 error are handled by the switch above
    default:
        return 0;
    }
}

void PrintErrors()
{
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR)
    {
        DebugFormat("GL Error: %s", GetErrorString(err));
    }
}

const char* GetGPUVendorName()
{
    return glGetString(GL_VENDOR);
}

const char* GetGPUName()
{
    return glGetString(GL_RENDERER);
}

void SetPerspectiveMatrix(float fovY, float aspect, float zNear, float zFar)
{
    const GLdouble pi = 3.1415926535897932384626433832795;
    GLdouble fW, fH;

    //fH = tan( (fovY / 2) / 180 * pi ) * zNear;
    fH = tan(fovY / 360 * pi) * zNear;
    fW = fH * aspect;

    glFrustum(-fW, fW, -fH, fH, zNear, zFar);
}

void ResizeViewport(int width, int height)
{
    if (height == 0)
    {
        height = 1;
    }

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    //glOrtho(-10, 10, -10, 10, 0, 100);
    SetPerspectiveMatrix(65.0f, (GLfloat)width / (GLfloat)height, 0.1f, 500.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

Matrix __modelview_Matrix;

void ClearScreen()
{
    PrintErrors();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Matrix mat = __modelview_Matrix;
    MatrixTranspose(&mat);
    MatrixInvert(&mat);
    glLoadMatrixf(&mat.m0);
}

void SetCameraTransformMatrix(Matrix matrix)
{
    __modelview_Matrix = matrix;
}

void SetCameraTransform(Transform transform)
{
    __modelview_Matrix = MatrixIdentity();
    __modelview_Matrix = MatrixMultiply(__modelview_Matrix,
        QuaternionToMatrix(transform.rotation));
    __modelview_Matrix = MatrixMultiply(__modelview_Matrix,
        MatrixTranslate(transform.position.x, transform.position.y, transform.position.z));
}

void Mesh_UploadToGPU(int meshID)
{
    Mesh mesh = Mesh_Get(meshID);
    if (mesh.vertexArray == 0 || mesh.indexArray == 0)
        return;

    glGenBuffers(1, &mesh.gpuVertexBuffer);
    glGenBuffers(1, &mesh.gpuIndexBuffer);

    glBindBuffer(GL_ARRAY_BUFFER, mesh.gpuVertexBuffer);
    {
        //glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * mesh.vertexCount, mesh.vertexArray, GL_STATIC_DRAW);

        int verticesSize = sizeof(Vertex) * mesh.vertexCount;
        int uvSize = 0;

        if (mesh.uvArray != 0)
            uvSize = sizeof(UV) * mesh.vertexCount;

        glBufferData(GL_ARRAY_BUFFER, verticesSize + uvSize, 0, GL_STATIC_DRAW);

        // Vertices sub data
        glBufferSubData(GL_ARRAY_BUFFER, 0, verticesSize, mesh.vertexArray);

        unsigned int offset = verticesSize;

        // UV sub data
        if (uvSize != 0)
        {
            glBufferSubData(GL_ARRAY_BUFFER, offset, uvSize, mesh.uvArray);
            offset += uvSize;
        }
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.gpuIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(VertexPtrType) * mesh.indexCount, mesh.indexArray, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    printf("Mesh_UploadToGPU %i %i\n", mesh.gpuVertexBuffer, mesh.gpuIndexBuffer);
    Mesh_SetGPUBuffers(meshID, mesh.gpuVertexBuffer, mesh.gpuIndexBuffer);
    // TODO: handle error
}

void Mesh_DeleteFromGPU(int meshID)
{
    Mesh mesh = Mesh_Get(meshID);
    glDeleteBuffers(1, &mesh.gpuVertexBuffer);
    glDeleteBuffers(1, &mesh.gpuIndexBuffer);
}

void Draw_Quad(float x, float y, float width, float height)
{

    glBegin(GL_TRIANGLES);

    glColor4f(1, 1, 1, 1);
    glTranslatef(x, y, -1);

    glVertex3f(-width, -height, -1);
    glVertex3f(width, -height, -1);
    glVertex3f(width, height, -1);

    glVertex3f(width, height, -1);
    glVertex3f(-width, height, -1);
    glVertex3f(-width, -height, -1);

    glEnd();

    //glBegin(GL_QUADS);
    //
    //glColor4f(1, 1, 1, 1);
    //glTranslatef(x, y, -1);
    //glVertex3f(x + -width, -height, -1);
    //glVertex3f(x + width, -height, -1);
    //glVertex3f(x + width, height, -1);
    //glVertex3f(x + -width, height, -1);
    //
    //glEnd();
}

void Draw_Mesh(int meshID, Transform transform)
{
    Mesh mesh = Mesh_Get(meshID);

    if (mesh.vertexArray == 0)
    {
        DebugLine("Mesh has no vertices");
        return;
    }

    if (mesh.gpuVertexBuffer == 0 || mesh.gpuIndexBuffer == 0)
    {
        DebugFormat("Mesh not uploaded to GPU! %u %u", mesh.gpuVertexBuffer, mesh.gpuIndexBuffer);
        return;
    }

    glColor4f(1, 1, 1, 1);
    glPushMatrix();

    glTranslatef(transform.position.x, transform.position.y, transform.position.z);

    Vector3 rot = QuaternionToEuler(transform.rotation);

    glRotatef(rot.x, 1, 0, 0);
    glRotatef(rot.y, 0, 1, 0);
    glRotatef(rot.z, 0, 0, 1);
    glScalef(transform.scale.x, transform.scale.y, transform.scale.z);

    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    glBindBuffer(GL_ARRAY_BUFFER, mesh.gpuVertexBuffer);

    // Vertex data at location 0
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    // Default location for UV attribute == 8 on NVidia gpu: 
    // https://stackoverflow.com/questions/20573235/what-are-the-attribute-locations-for-fixed-function-pipeline-in-opengl-4-0-cor
    const int uvAttribLoc = 8;
    if (mesh.uvArray != 0)
    {
        glEnableVertexAttribArray(uvAttribLoc);
        glVertexAttribPointer(uvAttribLoc, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(Vertex) * mesh.vertexCount));
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.gpuIndexBuffer);
    glDrawElements(GL_TRIANGLES, mesh.indexCount, GL_UNSIGNED_SHORT, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(uvAttribLoc);

    //glEnableClientState(GL_VERTEX_ARRAY);
    //glVertexPointer(3, GL_FLOAT, 0, mesh.vertexArray);
    //
    //
    //if (mesh.uvArray == 0)
    //{
    //    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    //}
    //else
    //{
    //    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //    glTexCoordPointer(2, GL_FLOAT, 0, mesh.uvArray);
    //}
    //
    //glDrawElements(GL_TRIANGLES, mesh.indexCount, GL_UNSIGNED_SHORT, mesh.indexArray);

    glPopMatrix();
}

typedef struct
{
    float x;
    float y;
    float z;
    unsigned char color[4];
} TextVertex;

void Draw_Text(float x, float y, char* text)
{
    TextVertex buf[256];

    int quadcount = stb_easy_font_print(x, y, text, 0, buf, 256*sizeof(TextVertex));

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    int width;
    int height;
    GetScreenSize(&width, &height);

    glOrtho(0, width/2, height/2, 0, -10, 100);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glBegin(GL_QUADS);

    for (int i = 0; i < quadcount*4; i ++)
    {
        glVertex3f(buf[i].x, buf[i].y, buf[i].z);
    }

    glEnd();

    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}
