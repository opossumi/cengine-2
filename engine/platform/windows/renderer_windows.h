#ifndef _RENDERER_WINDOWS_H_
#define _RENDERER_WINDOWS_H_

int InitRenderer(HWND hwnd, HGLRC* hglrc);
void DeinitRenderer(HWND hwnd, HGLRC hglrc);

#endif